# Cours de M2 MIAGE 2017/2018 #

### Cours ###

AOC - Architectures à objets canoniques, Noël PLOUZEAU  
EDD - Entrepôt De Données, Marc BOUSSE  
IDE - Ingénierie Des Exigences, Sonia MUINA  
IDM - Ingénierie Des Modèles, Jean-Marc JEZEQUEL / Mathieu ACHER  
MDP - Management De Production, Nicolas VIGNERON / SORRE  
MPI - Management de projet et intégration d'applications, Ludovic ROUTIER  
