<?xml version="1.0" encoding="UTF-8"?>
<?PowerDesigner AppLocale="UTF16" ID="{66D1462C-CC74-4FA9-B3B9-D5BAC0BAFBF9}" Label="Examen EDD 1ère session 2010-2011" LastModificationDate="1505808017" Name="InfosComm" Objects="50" Symbols="31" Target="Java" TargetLink="Reference" Type="{18112060-1A4B-11D1-83D9-444553540000}" signature="CLD_OBJECT_MODEL" version="15.0.0.2613"?>
<!-- Veuillez ne pas modifier ce fichier -->

<Model xmlns:a="attribute" xmlns:c="collection" xmlns:o="object">

<o:RootObject Id="o1">
<c:Children>
<o:Model Id="o2">
<a:ObjectID>66D1462C-CC74-4FA9-B3B9-D5BAC0BAFBF9</a:ObjectID>
<a:Name>InfosComm</a:Name>
<a:Code>InfosComm</a:Code>
<a:CreationDate>1323790425</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323849629</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Comment>Examen EDD 1ère session 2010-2011</a:Comment>
<a:PackageOptionsText>[FolderOptions]

[FolderOptions\Class Diagram Objects]
GenerationCheckModel=Yes
GenerationPath=
GenerationOptions=
GenerationTasks=
GenerationTargets=
GenerationSelections=

[FolderOptions\CheckModel]

[FolderOptions\CheckModel\Package]

[FolderOptions\CheckModel\Package\Circular inheritance]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Package\Circular dependency]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Package\ShortcutUniqCode]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Package\ChildShortcut]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe]

[FolderOptions\CheckModel\Classe\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Empty classifier]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Persistent class]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Association Identifier]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Visibility]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Constructor return type]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Constructor modifier]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Method implementation]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Role name assignment]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Role name unicity]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanInfo]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\JavaBean]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Inheritance on Enum Type (Java)]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassDefinition]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassBusinessImpl]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassHomeImpl]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbCreate]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbPostCreate]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbFind]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbHome]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\BeanClassEjbSelect]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\PKClassDefinition]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\PKClassAttributes]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\PKClassExistence]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Mapping]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\MappingSFMap]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\CsfrWrongBound]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\ClssInvalidGenMode]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Java_Class_Enum_Enum_ration_abstraite]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Java_Class_Enum_Enum_ration_finale]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe\Java_Class_EJB3BeanClass_Composant_EJB_manquant]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface]

[FolderOptions\CheckModel\Interface\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Empty classifier]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Visibility]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Interface constructor]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Association navigability]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\HomeCreateMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\HomeFindMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\HomeMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\ObjectBusinessMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\CsfrWrongBound]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut]

[FolderOptions\CheckModel\Classe.Attribut\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\CheckNumParam]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\Datatype assignment]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\Extend final class]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut\DomainDivergence]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant]

[FolderOptions\CheckModel\Classe.Identifiant\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant\EmptyColl - ATTR]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant\CheckIncludeColl - Clss]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut]

[FolderOptions\CheckModel\Interface.Attribut\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\CheckNumParam]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\Datatype assignment]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\Extend final class]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\DomainDivergence]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\Event parameter datatype]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération]

[FolderOptions\CheckModel\Classe.Opération\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Return type assignment]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Parameter datatype]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Abstract operation&#39;s body]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Abstract operation]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Operation signature]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Overriding operation]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Enum Abstract Methods]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération\Java_Operation_Param_tre_d_argument_variable]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Port]

[FolderOptions\CheckModel\Classe.Port\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Port\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Port\PortIsolated]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Partie]

[FolderOptions\CheckModel\Classe.Partie\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Partie\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Partie\PartLink]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Partie\PartComposition]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage]

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\AscnNull]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\AscnIntf]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Interface.Opération]

[FolderOptions\CheckModel\Interface.Opération\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\Return type assignment]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\Parameter datatype]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\Java_Operation_Param_tre_d_argument_variable]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Association]

[FolderOptions\CheckModel\Association\Generic links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Association\Bound links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation]

[FolderOptions\CheckModel\Généralisation\Redundant Generalizations]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Multiple inheritance (Java)]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Final datatype with initial value]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Non-Persistent Specifying Attribute]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Generic links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Généralisation\Bound links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réalisation]

[FolderOptions\CheckModel\Réalisation\Redundant Realizations]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réalisation\Generic links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réalisation\Bound links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine]

[FolderOptions\CheckModel\Domaine\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine\CheckNumParam]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Acteur]

[FolderOptions\CheckModel\Acteur\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Acteur\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Acteur\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Cas d&#39;utilisation]

[FolderOptions\CheckModel\Cas d&#39;utilisation\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Cas d&#39;utilisation\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Cas d&#39;utilisation\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet]

[FolderOptions\CheckModel\Objet\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Lien entre objets]

[FolderOptions\CheckModel\Lien entre objets\Redundant Instance links]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction]

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefDiagram]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefLifelines]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefInpMsg]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefOutMsg]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Message]

[FolderOptions\CheckModel\Message\MessageNoNumber]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Message\MessageManyLinks]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Message\Actor-Message]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité]

[FolderOptions\CheckModel\Activité\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité\CheckActvTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité\CheckNoStart]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité\CheckActvReuse]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud d&#39;objet]

[FolderOptions\CheckModel\Noeud d&#39;objet\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud d&#39;objet\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud d&#39;objet\CheckObndDttp]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Décision]

[FolderOptions\CheckModel\Décision\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Décision\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Décision\CheckDcsnCompl]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Synchronisation]

[FolderOptions\CheckModel\Synchronisation\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Synchronisation\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Synchronisation\CheckSyncCompl]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Unité d&#39;organisation]

[FolderOptions\CheckModel\Unité d&#39;organisation\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Unité d&#39;organisation\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Unité d&#39;organisation\CheckPrntOrgnLoop]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Début]

[FolderOptions\CheckModel\Début\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Début\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Début\CheckStrtTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fin]

[FolderOptions\CheckModel\Fin\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fin\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fin\CheckStrtTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Transition]

[FolderOptions\CheckModel\Transition\CheckTrnsSrc]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Transition\CheckTrnsCond]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Transition\TrnsDuplSTAT]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux]

[FolderOptions\CheckModel\Flux\CheckFlowSrc]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux\CheckFlowNoCond]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux\CheckFlowCond]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux\FlowDuplOOMACTV]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Evénement]

[FolderOptions\CheckModel\Evénement\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Evénement\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Evénement\EvntUnused]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat]

[FolderOptions\CheckModel\Etat\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\StatTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\StatNoStart]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\ActnOrder]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action]

[FolderOptions\CheckModel\Etat.Action\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action\ActnEvent]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action\ActnDupl]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Point de jonction]

[FolderOptions\CheckModel\Point de jonction\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Point de jonction\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Point de jonction\JnPtCompl]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant]

[FolderOptions\CheckModel\Composant\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\EJBClassifiers]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\Method Soap Message redefinition]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\WSDLJava_Component_Service_Web_vide]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Port]

[FolderOptions\CheckModel\Composant.Port\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Port\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Port\PortIsolated]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage]

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\AscnNull]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\AscnIntf]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Noeud]

[FolderOptions\CheckModel\Noeud\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud\Empty Node]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant]

[FolderOptions\CheckModel\Instance de composant\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\Component Instance with null Component]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\Duplicate Component Instance]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\Isolated Component Instance]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données]

[FolderOptions\CheckModel\Source de données\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données\EmptyColl - MODLSRC]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données\Data Source Target Consistency]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre d&#39;entrée]

[FolderOptions\CheckModel\Activité.Paramètre d&#39;entrée\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre d&#39;entrée\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre de sortie]

[FolderOptions\CheckModel\Activité.Paramètre de sortie\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre de sortie\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réplication]

[FolderOptions\CheckModel\Réplication\PartialReplication]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Règle de gestion]

[FolderOptions\CheckModel\Règle de gestion\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Règle de gestion\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Règle de gestion\EmptyColl - OBJCOL]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet étendu]

[FolderOptions\CheckModel\Objet étendu\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet étendu\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Lien étendu]

[FolderOptions\CheckModel\Lien étendu\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Lien étendu\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fichier]

[FolderOptions\CheckModel\Fichier\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fichier\CheckPathExists]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes</a:PackageOptionsText>
<a:ModelOptionsText>[ModelOptions]

[ModelOptions\Cld]
CaseSensitive=Yes
DisplayName=Yes
EnableTrans=Yes
EnableRequirements=No
ShowClss=No
DeftAttr=int
DeftMthd=int
DeftParm=int
DeftCont=java.util.Collection
DomnDttp=Yes
DomnChck=No
DomnRule=No
SupportDelay=No
PreviewEditable=Yes
AutoRealize=No
DttpFullName=Yes
DeftClssAttrVisi=private
VBNetPreprocessingSymbols=
CSharpPreprocessingSymbols=

[ModelOptions\Cld\NamingOptionsTemplates]

[ModelOptions\Cld\ClssNamingOptions]

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG]

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN]

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS]

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,FirstUpperChar)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDINTF]

[ModelOptions\Cld\ClssNamingOptions\CLDINTF\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,FirstUpperChar)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDINTF\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\UCDACTR]

[ModelOptions\Cld\ClssNamingOptions\UCDACTR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\UCDACTR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS]

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT]

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG]

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP]

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,FirstUpperChar)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDATTR]

[ModelOptions\Cld\ClssNamingOptions\CLDATTR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDATTR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD]

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDPARM]

[ModelOptions\Cld\ClssNamingOptions\CLDPARM\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDPARM\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\OOMPORT]

[ModelOptions\Cld\ClssNamingOptions\OOMPORT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\OOMPORT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\OOMPART]

[ModelOptions\Cld\ClssNamingOptions\OOMPART\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\OOMPART\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDASSC]

[ModelOptions\Cld\ClssNamingOptions\CLDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\CLDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\UCDASSC]

[ModelOptions\Cld\ClssNamingOptions\UCDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\UCDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK]

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\RQLINK]

[ModelOptions\Cld\ClssNamingOptions\RQLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\RQLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK]

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK]

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\OOMACTV]

[ModelOptions\Cld\ClssNamingOptions\OOMACTV\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\OOMACTV\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\ACDOBST]

[ModelOptions\Cld\ClssNamingOptions\ACDOBST\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\ACDOBST\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\STAT]

[ModelOptions\Cld\ClssNamingOptions\STAT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\STAT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\DPDNODE]

[ModelOptions\Cld\ClssNamingOptions\DPDNODE\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\DPDNODE\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI]

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\DPDASSC]

[ModelOptions\Cld\ClssNamingOptions\DPDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\DPDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\OOMVAR]

[ModelOptions\Cld\ClssNamingOptions\OOMVAR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\OOMVAR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\FILO]

[ModelOptions\Cld\ClssNamingOptions\FILO\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=&quot;\/:*?&lt;&gt;|&quot;
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\FILO\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_. &quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ]

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\FRMELNK]

[ModelOptions\Cld\ClssNamingOptions\FRMELNK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\FRMELNK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\DefaultClass]

[ModelOptions\Cld\ClssNamingOptions\DefaultClass\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=

[ModelOptions\Cld\ClssNamingOptions\DefaultClass\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=

[ModelOptions\Generate]

[ModelOptions\Generate\Cdm]
CheckModel=Yes
SaveLinks=Yes
NameToCode=No
Notation=2

[ModelOptions\Generate\Pdm]
CheckModel=Yes
SaveLinks=Yes
ORMapping=No
NameToCode=No
BuildTrgr=No
TablePrefix=
RefrUpdRule=Restrict
RefrDelRule=Restrict
IndxPKName=%TABLE%_PK
IndxAKName=%TABLE%_AK
IndxFKName=%REFR%_FK
IndxThreshold=
ColnFKName=%.3:PARENT%_%COLUMN%
ColnFKNameUse=No
PreserveMode=Yes
EnableTransformations=No

[ModelOptions\Generate\Xsm]
CheckModel=Yes
SaveLinks=Yes
ORMapping=No
NameToCode=No</a:ModelOptionsText>
<a:Author>M. Bousse</a:Author>
<a:Version>1.1</a:Version>
<c:GeneratedModels>
<o:Shortcut Id="o3">
<a:ObjectID>29B50108-454A-48F0-926F-09ECEC467A59</a:ObjectID>
<a:Name>InfosComm</a:Name>
<a:Code>INFOSCOMM</a:Code>
<a:CreationDate>1323795866</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323795866</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>67E9805F-90D9-4906-8C0C-65204ED76FD0</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
<o:Shortcut Id="o4">
<a:ObjectID>0326BB7C-E387-417D-B682-EBD4CAC0DD60</a:ObjectID>
<a:Name>InfosCommAvec01</a:Name>
<a:Code>INFOSCOMM_01</a:Code>
<a:CreationDate>1457519532</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1457519532</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>F7BDC74F-411A-447F-88F6-CBE30196CB19</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
</c:GeneratedModels>
<c:ObjectLanguage>
<o:Shortcut Id="o5">
<a:ObjectID>BD33283F-CD97-4415-BEC2-1A7BD62F501B</a:ObjectID>
<a:Name>Java</a:Name>
<a:Code>Java</a:Code>
<a:CreationDate>1323790421</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323790421</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>0DEDDB90-46E2-45A0-886E-411709DA0DC9</a:TargetID>
<a:TargetClassID>1811206C-1A4B-11D1-83D9-444553540000</a:TargetClassID>
</o:Shortcut>
</c:ObjectLanguage>
<c:ExtendedModelDefinitions>
<o:Shortcut Id="o6">
<a:ObjectID>EC70EFF4-A072-4104-938D-1F5C5468C004</a:ObjectID>
<a:Name>WSDL for Java</a:Name>
<a:Code>WSDLJava</a:Code>
<a:CreationDate>1323790425</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323790425</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>C8F5F7B2-CF9D-4E98-8301-959BB6E86C8A</a:TargetID>
<a:TargetClassID>186C8AC3-D3DC-11D3-881C-00508B03C75C</a:TargetClassID>
</o:Shortcut>
</c:ExtendedModelDefinitions>
<c:ClassDiagrams>
<o:ClassDiagram Id="o7">
<a:ObjectID>65E00F5D-04B7-4B84-A1CF-CB572403678C</a:ObjectID>
<a:Name>DiagrammeClasses_1</a:Name>
<a:Code>DiagrammeClasses_1</a:Code>
<a:CreationDate>1323790425</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505808017</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\CLD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=Yes
Grid size=0
Graphic unit=2
Window color=255, 255, 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255, 255, 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Mode=2
Trunc Length=40
Word Length=40
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
File Location=No
PckgShowStrn=Yes
Package.Comment=No
Package.IconPicture=No
Package_SymbolLayout=
Display Model Version=Yes
Class.IconPicture=No
Class_SymbolLayout=
Interface.IconPicture=No
Interface_SymbolLayout=
Port.IconPicture=No
Port_SymbolLayout=
FileObject.IconPicture=No
FileObject_SymbolLayout=
ClssShowSttr=Yes
Class.Comment=No
ClssShowCntr=Yes
ClssShowAttr=Yes
ClssAttrTrun=No
ClssAttrMax=3
ClssShowMthd=Yes
ClssMthdTrun=No
ClssMthdMax=3
ClssShowInnr=Yes
IntfShowSttr=Yes
Interface.Comment=No
IntfShowAttr=Yes
IntfAttrTrun=No
IntfAttrMax=3
IntfShowMthd=Yes
IntfMthdTrun=No
IntfMthdMax=3
IntfShowCntr=Yes
IntfShowInnr=Yes
PortShowName=Yes
PortShowType=No
PortShowMult=No
AttrShowVisi=Yes
AttrVisiFmt=1
AttrShowStrn=Yes
AttrShowDttp=Yes
AttrShowDomn=No
AttrShowInit=Yes
MthdShowVisi=Yes
MthdVisiFmt=1
MthdShowStrn=Yes
MthdShowRttp=Yes
MthdShowParm=Yes
AsscShowName=No
AsscShowCntr=Yes
AsscShowRole=Yes
AsscShowOrdr=Yes
AsscShowMult=Yes
AsscMultStr=Yes
AsscShowStrn=No
GnrlShowName=No
GnrlShowStrn=Yes
GnrlShowCntr=Yes
RlzsShowName=No
RlzsShowStrn=Yes
RlzsShowCntr=Yes
DepdShowName=No
DepdShowStrn=Yes
DepdShowCntr=Yes
RqlkShowName=No
RqlkShowStrn=Yes
RqlkShowCntr=Yes

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LABLFont=Arial,8,N
LABLFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0, 0, 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDCLASS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
ClassPrimaryAttributeFont=Arial,8,U
ClassPrimaryAttributeFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDINTF]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMPORT]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=250 241 211
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDASSC]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
MULAFont=Arial,8,N
MULAFont color=0 0 0
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\INNERLINK]
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDACLK]
Line style=2
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RLZSLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
Line style=2
Pen=3 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RQLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
Line style=2
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
NAMEFont=Arial,8,N
NAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:TitleSymbol Id="o8">
<a:Text>Version: 1.1</a:Text>
<a:CreationDate>1323795380</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:Rect>((-22941,14820), (-9067,20669))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:LineColor>16512</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:TitleSymbol>
<o:AssociationSymbol Id="o9">
<a:CreationDate>1323791144</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:SourceTextOffset>(937, 163)</a:SourceTextOffset>
<a:DestinationTextOffset>(-1087, -388)</a:DestinationTextOffset>
<a:Rect>((-2635,19365), (1338,29115))</a:Rect>
<a:ListOfPoints>((-611,29115),(-611,19365))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o10"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o11"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o12"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o13">
<a:CreationDate>1323791145</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:SourceTextOffset>(1012, 238)</a:SourceTextOffset>
<a:DestinationTextOffset>(-1087, -13)</a:DestinationTextOffset>
<a:Rect>((-2635,9712), (1413,19365))</a:Rect>
<a:ListOfPoints>((-611,19365),(-611,9712))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o11"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o14"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o15"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o16">
<a:CreationDate>1323791225</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:DestinationTextOffset>(-1537, -88)</a:DestinationTextOffset>
<a:Rect>((-6761,-5208), (5539,9712))</a:Rect>
<a:ListOfPoints>((-611,9712),(-611,-5208))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o14"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o17"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o18"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o19">
<a:CreationDate>1323791965</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:SourceTextOffset>(37, 1037)</a:SourceTextOffset>
<a:Rect>((-611,-6132), (17964,-3658))</a:Rect>
<a:ListOfPoints>((-611,-5208),(17964,-5208))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o17"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o20"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o21"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o22">
<a:CreationDate>1323794236</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:SourceTextOffset>(787, 463)</a:SourceTextOffset>
<a:DestinationTextOffset>(-937, -388)</a:DestinationTextOffset>
<a:Rect>((16090,4520), (19763,14495))</a:Rect>
<a:ListOfPoints>((17964,14495),(17964,4520))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o23"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o24"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o25"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o26">
<a:CreationDate>1323794237</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:DestinationTextOffset>(-1012, -463)</a:DestinationTextOffset>
<a:Rect>((16015,-5208), (19988,4520))</a:Rect>
<a:ListOfPoints>((17964,4520),(17964,-5208))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o24"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o20"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o27"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o28">
<a:CreationDate>1323794239</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:SourceTextOffset>(1087, 538)</a:SourceTextOffset>
<a:DestinationTextOffset>(-1162, 62)</a:DestinationTextOffset>
<a:Rect>((15790,-16217), (19988,-5208))</a:Rect>
<a:ListOfPoints>((17964,-5208),(17964,-16217))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o20"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o29"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o30"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o31">
<a:CreationDate>1323794393</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:SourceTextOffset>(450, -3975)</a:SourceTextOffset>
<a:Rect>((-17480,-10107), (-611,-5108))</a:Rect>
<a:ListOfPoints>((-17480,-5208),(-611,-5208))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o32"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o17"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o33"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o34">
<a:CreationDate>1323794395</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:SourceTextOffset>(-938, 313)</a:SourceTextOffset>
<a:DestinationTextOffset>(1088, -13)</a:DestinationTextOffset>
<a:Rect>((-19355,-16142), (-15380,-5208))</a:Rect>
<a:ListOfPoints>((-17480,-5208),(-17480,-16142))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o32"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o35"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o36"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o37">
<a:CreationDate>1323794816</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:Rect>((52511,-3518), (63685,14262))</a:Rect>
<a:ListOfPoints>((58098,14262),(58098,-3518))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o38"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o39"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o40"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o41">
<a:CreationDate>1323794821</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:DestinationTextOffset>(1388, -163)</a:DestinationTextOffset>
<a:Rect>((55061,-4268), (65785,13931))</a:Rect>
<a:ListOfPoints>((60423,13931),(60423,-4268))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o38"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o39"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o42"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o43">
<a:CreationDate>1323794823</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:DestinationTextOffset>(-1837, -13)</a:DestinationTextOffset>
<a:Rect>((50191,-5280), (61215,14328))</a:Rect>
<a:ListOfPoints>((55703,14328),(55703,-5280))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o38"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o39"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o44"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o45">
<a:CreationDate>1323794975</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:SourceTextOffset>(-638, -661)</a:SourceTextOffset>
<a:DestinationTextOffset>(113, 300)</a:DestinationTextOffset>
<a:Rect>((53727,-23958), (61601,-5355))</a:Rect>
<a:ListOfPoints>((57551,-5355),(57551,-23958))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o39"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o46"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o47"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o48">
<a:CreationDate>1323794977</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:SourceTextOffset>(1612, 388)</a:SourceTextOffset>
<a:Rect>((54005,-24421), (64504,-2543))</a:Rect>
<a:ListOfPoints>((61955,-2543),(61955,-24421))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o39"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o46"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o49"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o50">
<a:CreationDate>1323794978</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:SourceTextOffset>(-1163, 463)</a:SourceTextOffset>
<a:DestinationTextOffset>(-4387, 2400)</a:DestinationTextOffset>
<a:Rect>((46408,-24421), (54682,-6968))</a:Rect>
<a:ListOfPoints>((54582,-6968),(54582,-24421))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o39"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o46"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o51"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o52">
<a:CreationDate>1323795510</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:Rect>((34220,-6431), (57793,-4482))</a:Rect>
<a:ListOfPoints>((34220,-5408),(57793,-5408))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o53"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o39"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o54"/>
</c:Object>
</o:AssociationSymbol>
<o:ClassSymbol Id="o17">
<a:CreationDate>1323790436</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-6235,-8057), (5013,-2359))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:Class Ref="o55"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o14">
<a:CreationDate>1323790519</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-3263,7463), (2041,11962))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>15789759</a:FillColor>
<a:ShadowColor>15789759</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:ClonePredecessor>
<o:ClassSymbol Ref="o38"/>
</c:ClonePredecessor>
<c:Object>
<o:Class Ref="o56"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o11">
<a:CreationDate>1323791099</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-3417,17116), (2195,21615))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>15789759</a:FillColor>
<a:ShadowColor>15789759</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o57"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o10">
<a:CreationDate>1323791102</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-3726,26866), (2504,31365))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>15789759</a:FillColor>
<a:ShadowColor>15789759</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o58"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o20">
<a:CreationDate>1323791354</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((12261,-7458), (23667,-2959))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>15789759</a:FillColor>
<a:ShadowColor>15789759</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:ClonePredecessor>
<o:ClassSymbol Ref="o46"/>
</c:ClonePredecessor>
<c:Object>
<o:Class Ref="o59"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o24">
<a:CreationDate>1323794175</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((12802,2271), (23126,6770))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>15789759</a:FillColor>
<a:ShadowColor>15789759</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o60"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o23">
<a:CreationDate>1323794179</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((11257,12246), (24671,16745))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>15789759</a:FillColor>
<a:ShadowColor>15789759</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o61"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o29">
<a:CreationDate>1323794215</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((13922,-18467), (22006,-13968))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>15789759</a:FillColor>
<a:ShadowColor>15789759</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o62"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o32">
<a:CreationDate>1323794333</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-23530,-7458), (-11430,-2959))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>15789759</a:FillColor>
<a:ShadowColor>15789759</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:ClonePredecessor>
<o:ClassSymbol Ref="o53"/>
</c:ClonePredecessor>
<c:Object>
<o:Class Ref="o63"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o35">
<a:CreationDate>1323794336</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-23376,-18392), (-11584,-13893))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>15789759</a:FillColor>
<a:ShadowColor>15789759</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o64"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o39">
<a:CreationDate>1323794660</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((52169,-8204), (63417,-2506))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:Class Ref="o65"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o38">
<a:CreationDate>1323794785</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((55462,12013), (60766,16512))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>15789759</a:FillColor>
<a:ShadowColor>15789759</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o56"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o46">
<a:CreationDate>1323794953</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((52636,-26208), (64042,-21709))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>15789759</a:FillColor>
<a:ShadowColor>15789759</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o59"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o53">
<a:CreationDate>1323795477</a:CreationDate>
<a:ModificationDate>1505807902</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((28170,-7705), (40270,-3206))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>15789759</a:FillColor>
<a:ShadowColor>15789759</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o63"/>
</c:Object>
</o:ClassSymbol>
</c:Symbols>
</o:ClassDiagram>
</c:ClassDiagrams>
<c:DefaultDiagram>
<o:ClassDiagram Ref="o7"/>
</c:DefaultDiagram>
<c:Classes>
<o:Class Id="o55">
<a:ObjectID>3FDFC22B-3831-400C-A2DE-91EC44AAA838</a:ObjectID>
<a:Name>Publications</a:Name>
<a:Code>Publications</a:Code>
<a:CreationDate>1323790436</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732741</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Cube</a:Stereotype>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o66">
<a:ObjectID>9A4736FF-5263-4AA7-A50F-3A758546E189</a:ObjectID>
<a:Name>nb. de publications</a:Name>
<a:Code>nbPublications</a:Code>
<a:CreationDate>1323790462</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323794749</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>int</a:DataType>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o56">
<a:ObjectID>F7B05957-16C9-45C7-9A14-C62649B6F55C</a:ObjectID>
<a:Name>Jour</a:Name>
<a:Code>Jour</a:Code>
<a:CreationDate>1323790519</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732911</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Level</a:Stereotype>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o67">
<a:ObjectID>9F586A2E-21D2-4F9F-B3DC-DB3B80939DFF</a:ObjectID>
<a:Name>jour</a:Name>
<a:Code>jour</a:Code>
<a:CreationDate>1323791039</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323791048</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>int</a:DataType>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o57">
<a:ObjectID>BA804509-71FF-47D5-87DA-653158F17848</a:ObjectID>
<a:Name>Mois</a:Name>
<a:Code>Mois</a:Code>
<a:CreationDate>1323791099</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732931</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Level</a:Stereotype>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o68">
<a:ObjectID>EB9528B9-4D92-4FDF-929A-49FD19367625</a:ObjectID>
<a:Name>mois</a:Name>
<a:Code>mois</a:Code>
<a:CreationDate>1323791099</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323791123</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>int</a:DataType>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o58">
<a:ObjectID>6165217D-92C5-4671-9040-0899456A842C</a:ObjectID>
<a:Name>Année</a:Name>
<a:Code>Annee</a:Code>
<a:CreationDate>1323791102</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732886</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Level</a:Stereotype>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o69">
<a:ObjectID>5ECF88C1-104E-4F7B-AA73-BF18A090FDBE</a:ObjectID>
<a:Name>année</a:Name>
<a:Code>annee</a:Code>
<a:CreationDate>1323791102</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323791133</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>int</a:DataType>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o59">
<a:ObjectID>9A2E26E7-EF5E-4EDC-8C1A-FB3E59BA7C8A</a:ObjectID>
<a:Name>Membre</a:Name>
<a:Code>Membre</a:Code>
<a:CreationDate>1323791354</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732926</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Level</a:Stereotype>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o70">
<a:ObjectID>B44AF694-5411-4AB9-B1BF-07F85E245372</a:ObjectID>
<a:Name>pseudo</a:Name>
<a:Code>pseudo</a:Code>
<a:CreationDate>1323791354</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323849487</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>java.lang.String</a:DataType>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o60">
<a:ObjectID>6F462D6C-C884-4989-90E2-1080A0D5399F</a:ObjectID>
<a:Name>Ville</a:Name>
<a:Code>Ville</a:Code>
<a:CreationDate>1323794175</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732945</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Level</a:Stereotype>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o71">
<a:ObjectID>ABABC5F8-6B75-4B38-8007-117062770401</a:ObjectID>
<a:Name>ville</a:Name>
<a:Code>ville</a:Code>
<a:CreationDate>1323794175</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323849487</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>java.lang.String</a:DataType>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o61">
<a:ObjectID>88A28C51-CDAD-4AE0-A284-45DBBFBC72F1</a:ObjectID>
<a:Name>Département</a:Name>
<a:Code>Departement</a:Code>
<a:CreationDate>1323794179</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732904</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Level</a:Stereotype>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o72">
<a:ObjectID>4422B86B-3D03-4077-A14F-FD74744E3C5D</a:ObjectID>
<a:Name>département</a:Name>
<a:Code>departement</a:Code>
<a:CreationDate>1323794179</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323849487</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>java.lang.String</a:DataType>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o62">
<a:ObjectID>3E1F0549-BD4E-4988-9AD3-C4738C00DB21</a:ObjectID>
<a:Name>Ancienneté</a:Name>
<a:Code>Anciennete</a:Code>
<a:CreationDate>1323794215</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732878</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Level</a:Stereotype>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o73">
<a:ObjectID>C2104CBD-58B9-44BD-88CE-E02D21580326</a:ObjectID>
<a:Name>ancienneté</a:Name>
<a:Code>anciennete</a:Code>
<a:CreationDate>1323794215</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323794230</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>int</a:DataType>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o63">
<a:ObjectID>DCB42224-4A14-483E-8955-D636E91306DC</a:ObjectID>
<a:Name>Article</a:Name>
<a:Code>Article</a:Code>
<a:CreationDate>1323794333</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732898</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Level</a:Stereotype>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o74">
<a:ObjectID>BEFED758-9E31-4D5B-83C9-0DFC0388D675</a:ObjectID>
<a:Name>n° article</a:Name>
<a:Code>numArticle</a:Code>
<a:CreationDate>1323794333</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323849487</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>java.lang.String</a:DataType>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o64">
<a:ObjectID>D384CB31-D5D7-47BC-9418-93953C537BD1</a:ObjectID>
<a:Name>Rubrique</a:Name>
<a:Code>Rubrique</a:Code>
<a:CreationDate>1323794336</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732938</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Level</a:Stereotype>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o75">
<a:ObjectID>37E41A38-07F4-46F3-B9AC-5DA227AB85DE</a:ObjectID>
<a:Name>rubrique</a:Name>
<a:Code>rubrique</a:Code>
<a:CreationDate>1323794336</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323849487</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>java.lang.String</a:DataType>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o65">
<a:ObjectID>29850A66-B35A-49A3-9C4B-D9052926931A</a:ObjectID>
<a:Name>Lectures</a:Name>
<a:Code>Lectures</a:Code>
<a:CreationDate>1323794660</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732837</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Cube</a:Stereotype>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o76">
<a:ObjectID>7B7D573F-38A3-48F3-BE10-B56B410B84E5</a:ObjectID>
<a:Name>nb. de lectures</a:Name>
<a:Code>nbLectures</a:Code>
<a:CreationDate>1323794660</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323794738</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>int</a:DataType>
</o:Attribute>
<o:Attribute Id="o77">
<a:ObjectID>DFC4B38C-6E01-4F91-8854-88F3ABD77FC0</a:ObjectID>
<a:Name>nb. d&#39;invitations</a:Name>
<a:Code>nbInvitations</a:Code>
<a:CreationDate>1323794666</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323794738</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>int</a:DataType>
</o:Attribute>
<o:Attribute Id="o78">
<a:ObjectID>903EE60C-BA24-4433-AB77-D6339B10FA97</a:ObjectID>
<a:Name>taux de succès</a:Name>
<a:Code>tauxSucces</a:Code>
<a:CreationDate>1323794666</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323849487</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:DataType>double</a:DataType>
</o:Attribute>
</c:Attributes>
</o:Class>
</c:Classes>
<c:Associations>
<o:Association Id="o12">
<a:ObjectID>05DD404C-A863-441E-8302-A9982154B361</a:ObjectID>
<a:Name>Association_1</a:Name>
<a:Code>association1</a:Code>
<a:CreationDate>1323791144</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323791174</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o57"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o58"/>
</c:Object2>
</o:Association>
<o:Association Id="o15">
<a:ObjectID>B4D067CC-745B-45ED-8341-43B98117FBF7</a:ObjectID>
<a:Name>Association_2</a:Name>
<a:Code>association2</a:Code>
<a:CreationDate>1323791145</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323791216</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o56"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o57"/>
</c:Object2>
</o:Association>
<o:Association Id="o18">
<a:ObjectID>0633E8A0-B5FF-4670-AF05-D372E68885F5</a:ObjectID>
<a:Name>Association_3</a:Name>
<a:Code>association3</a:Code>
<a:CreationDate>1323791225</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323794607</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Dimension</a:Stereotype>
<a:RoleAName>Dimension &quot;Date de publication&quot;</a:RoleAName>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o55"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o56"/>
</c:Object2>
</o:Association>
<o:Association Id="o21">
<a:ObjectID>11A4120E-D82C-4C2A-A77C-EEF425C99E66</a:ObjectID>
<a:Name>Association_4</a:Name>
<a:Code>association4</a:Code>
<a:CreationDate>1323791965</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323794599</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:RoleBName>Dimension &quot;Auteur&quot;</a:RoleBName>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o59"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o55"/>
</c:Object2>
</o:Association>
<o:Association Id="o25">
<a:ObjectID>9502C440-2733-497D-AE16-D532B4678E7B</a:ObjectID>
<a:Name>Association_5</a:Name>
<a:Code>association5</a:Code>
<a:CreationDate>1323794236</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323794256</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o60"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o61"/>
</c:Object2>
</o:Association>
<o:Association Id="o27">
<a:ObjectID>228DA62E-E00D-4E8C-950C-8275C70D6838</a:ObjectID>
<a:Name>Association_6</a:Name>
<a:Code>association6</a:Code>
<a:CreationDate>1323794237</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323794266</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o59"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o60"/>
</c:Object2>
</o:Association>
<o:Association Id="o30">
<a:ObjectID>C8F26DA1-65C0-43AE-ACEE-FB820022CBE8</a:ObjectID>
<a:Name>Association_7</a:Name>
<a:Code>association7</a:Code>
<a:CreationDate>1323794239</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323794280</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o62"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o59"/>
</c:Object2>
</o:Association>
<o:Association Id="o33">
<a:ObjectID>F0C4EEA1-071C-49B2-8018-3DFDE9A5503F</a:ObjectID>
<a:Name>Association_8</a:Name>
<a:Code>association8</a:Code>
<a:CreationDate>1323794393</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732741</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Dimension</a:Stereotype>
<a:RoleAName>Dimension &quot;Article&quot;</a:RoleAName>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o55"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o63"/>
</c:Object2>
</o:Association>
<o:Association Id="o36">
<a:ObjectID>621A03A4-D512-4CD4-9BB7-715100DA702D</a:ObjectID>
<a:Name>Association_9</a:Name>
<a:Code>association9</a:Code>
<a:CreationDate>1323794395</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323794433</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o64"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o63"/>
</c:Object2>
</o:Association>
<o:Association Id="o40">
<a:ObjectID>3A447762-6F59-438C-8D1B-3F618364B2E9</a:ObjectID>
<a:Name>Association_10</a:Name>
<a:Code>association10</a:Code>
<a:CreationDate>1323794816</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732763</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Dimension</a:Stereotype>
<a:RoleAName>Dimension &quot;Date publication&quot;</a:RoleAName>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o65"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o56"/>
</c:Object2>
</o:Association>
<o:Association Id="o42">
<a:ObjectID>A9140160-1136-4744-8A11-2CD9DCA5F93A</a:ObjectID>
<a:Name>Association_11</a:Name>
<a:Code>association11</a:Code>
<a:CreationDate>1323794821</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732773</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Dimension</a:Stereotype>
<a:RoleAName>Dimension &quot;Date de lecture&quot;</a:RoleAName>
<a:RoleAMultiplicity>0..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o65"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o56"/>
</c:Object2>
</o:Association>
<o:Association Id="o44">
<a:ObjectID>A046EBC8-96FB-4FE7-BC0B-D3BFC5BB58DB</a:ObjectID>
<a:Name>Association_12</a:Name>
<a:Code>association12</a:Code>
<a:CreationDate>1323794823</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732782</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Dimension</a:Stereotype>
<a:RoleAName>Dimension &quot;Date d&#39;invitation&quot;</a:RoleAName>
<a:RoleAMultiplicity>0..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o65"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o56"/>
</c:Object2>
</o:Association>
<o:Association Id="o47">
<a:ObjectID>548E6BAD-8790-4043-8A90-7454E73E5BB2</a:ObjectID>
<a:Name>Association_13</a:Name>
<a:Code>association13</a:Code>
<a:CreationDate>1323794975</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732790</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Dimension</a:Stereotype>
<a:RoleBName>Dimension &quot;Invitant&quot;</a:RoleBName>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o59"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o65"/>
</c:Object2>
</o:Association>
<o:Association Id="o49">
<a:ObjectID>87B523B5-5F40-4138-B921-43C62A61DFD2</a:ObjectID>
<a:Name>Association_14</a:Name>
<a:Code>association14</a:Code>
<a:CreationDate>1323794977</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732822</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Dimension</a:Stereotype>
<a:RoleBName>Dimension &quot;Lecteur&quot;</a:RoleBName>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o59"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o65"/>
</c:Object2>
</o:Association>
<o:Association Id="o51">
<a:ObjectID>5A705AB6-C96F-4415-A103-64B13558C93E</a:ObjectID>
<a:Name>Association_15</a:Name>
<a:Code>association15</a:Code>
<a:CreationDate>1323794978</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732692</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Dimension</a:Stereotype>
<a:RoleBName>Dimension &quot;Auteur&quot;</a:RoleBName>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o59"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o65"/>
</c:Object2>
</o:Association>
<o:Association Id="o54">
<a:ObjectID>5942CBAF-28E3-4D21-AC1D-12BC55557508</a:ObjectID>
<a:Name>Association_16</a:Name>
<a:Code>association16</a:Code>
<a:CreationDate>1323795510</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1505732837</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:Stereotype>Dimension</a:Stereotype>
<a:RoleAName>Dimension &quot;Article&quot;</a:RoleAName>
<a:RoleAMultiplicity>1..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleBNavigability>0</a:RoleBNavigability>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o65"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o63"/>
</c:Object2>
</o:Association>
</c:Associations>
<c:TargetModels>
<o:TargetModel Id="o79">
<a:ObjectID>8EBF3042-7162-441E-B331-A20B0DD949FF</a:ObjectID>
<a:Name>Java</a:Name>
<a:Code>Java</a:Code>
<a:CreationDate>1323790421</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323790421</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:TargetModelURL>file:///C|/Program Files (x86)/PowerAMC15PourWine/Fichiers de ressources/Langages objet/java5-j2ee14.xol</a:TargetModelURL>
<a:TargetModelID>0DEDDB90-46E2-45A0-886E-411709DA0DC9</a:TargetModelID>
<a:TargetModelClassID>1811206C-1A4B-11D1-83D9-444553540000</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o5"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o80">
<a:ObjectID>9CA85096-CD9D-4C98-A269-EC508E002B9D</a:ObjectID>
<a:Name>WSDL for Java</a:Name>
<a:Code>WSDLJava</a:Code>
<a:CreationDate>1323790425</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323790425</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:TargetModelURL>file:///C|/Program Files (x86)/PowerAMC15PourWine/Fichiers de ressources/Définitions étendues de modèle/WSDLJ2EE.xem</a:TargetModelURL>
<a:TargetModelID>C8F5F7B2-CF9D-4E98-8301-959BB6E86C8A</a:TargetModelID>
<a:TargetModelClassID>186C8AC3-D3DC-11D3-881C-00508B03C75C</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o6"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o81">
<a:ObjectID>46DF414A-B817-40A8-898A-1658B4FB1934</a:ObjectID>
<a:Name>InfosComm</a:Name>
<a:Code>INFOSCOMM</a:Code>
<a:CreationDate>1323795866</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1323796262</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:TargetModelURL>file:///D|/home/bousse/mes documents/Miroir ISTIC/Decisionnel/EDD M2 Miage/Examen EDD déc. 2010 (presse en ligne)/InfosComm.mpd</a:TargetModelURL>
<a:TargetModelID>67E9805F-90D9-4906-8C0C-65204ED76FD0</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o3"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o82">
<a:ObjectID>EAC067AF-4FE4-4112-AF41-5CB459AD0E9B</a:ObjectID>
<a:Name>InfosCommAvec01</a:Name>
<a:Code>INFOSCOMM_01</a:Code>
<a:CreationDate>1457519532</a:CreationDate>
<a:Creator>bousse</a:Creator>
<a:ModificationDate>1457519532</a:ModificationDate>
<a:Modifier>bousse</a:Modifier>
<a:TargetModelID>F7BDC74F-411A-447F-88F6-CBE30196CB19</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o4"/>
</c:SessionShortcuts>
</o:TargetModel>
</c:TargetModels>
</o:Model>
</c:Children>
</o:RootObject>

</Model>