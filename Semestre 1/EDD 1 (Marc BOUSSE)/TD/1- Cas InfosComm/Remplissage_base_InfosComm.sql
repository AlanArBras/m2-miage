delete from InfosCommFaitsPublications;
delete from InfosCommFaitsLectures;
delete from InfosCommDimArticle;
delete from InfosCommDimDate;
delete from InfosCommDimMembre2;

insert into InfosCommDimMembre2 (num_seq_membre, pseudo, ville, departement, anciennete,
								debut_validite, num_version, fin_validite)
values
	(9,  'bebert35',  'Rennes',       '35', 0, '2012-11-04', 1, '2013-11-03'),
	(10, 'bebert35',  'Rennes',       '35', 1, '2013-11-04', 2, '2199-01-01'),
	(1,  'georgette', 'Rennes',       '35', 0, '2013-10-18', 1, '2199-01-01'),
	(11, 'jean22',    'Saint Brieuc', '22', 0, '2012-11-28', 1, '2013-11-27'),
	(4,  'jean22',    'Saint Brieuc', '22', 1, '2013-11-28', 2, '2199-01-01'),
	(12, 'jojo35',    'Rennes',       '35', 0, '2012-08-09', 1, '2013-08-08'),
	(13, 'jojo35',    'Rennes',       '35', 1, '2013-08-09', 2, '2014-08-08'),
	(8,  'jojo35',    'Rennes',       '35', 2, '2014-08-09', 3, '2199-01-01'),
	(5,  'paul22',    'Saint Brieuc', '22', 0, '2013-12-20', 1, '2199-01-01'),
	(3,  'titi22',    'Saint Brieuc', '22', 0, '2012-12-06', 1, '2013-12-05'),
	(6,  'titi22',    'Saint Brieuc', '22', 1, '2013-12-06', 2, '2014-02-16'),
	(14, 'titi22',    'Lannion',      '22', 1, '2014-02-17', 3, '2199-01-01'),
	(2,  'toto35',    'Rennes',       '35', 0, '2014-09-16', 1, '2199-01-01'),
	(7,  'tutu53',    'Rennes',       '35', 0, '2014-05-20', 1, '2199-01-01') ;

insert into InfosCommDimArticle values (1, 'CE1111', 'Chiens ecrases') ;
insert into InfosCommDimArticle values (2, 'CE2222', 'Chiens ecrases') ;
insert into InfosCommDimArticle values (3, 'CE3333', 'Chiens ecrases') ;
insert into InfosCommDimArticle values (4, 'CS1111', 'Chats secourus') ;
insert into InfosCommDimArticle values (5, 'CS2222', 'Chats secourus') ;
insert into InfosCommDimArticle values (6, 'CS3333', 'Chats secourus') ;
insert into InfosCommDimArticle values (7, 'VM3333', 'Vaches melancoliques') ;
insert into InfosCommDimArticle values (8, 'VM7777', 'Vaches melancoliques') ;
insert into InfosCommDimArticle values (9, 'VM8888', 'Vaches melancoliques') ;
insert into InfosCommDimArticle values (10, 'VM9999', 'Vaches melancoliques') ;

insert into InfosCommDimDate values (1, 1, 10, 2011) ;
insert into InfosCommDimDate values (2, 2, 10, 2011) ;
insert into InfosCommDimDate values (3, 3, 10, 2011) ;
insert into InfosCommDimDate values (4, 13, 10, 2011) ;
insert into InfosCommDimDate values (5, 14, 10, 2011) ;
insert into InfosCommDimDate values (6, 15, 10, 2011) ;
insert into InfosCommDimDate values (7, 16, 10, 2011) ;
insert into InfosCommDimDate values (8, 17, 10, 2011) ;
insert into InfosCommDimDate values (9, 1, 11, 2011) ;
insert into InfosCommDimDate values (10, 2, 11, 2011) ;
insert into InfosCommDimDate values (11, 3, 11, 2011) ;
insert into InfosCommDimDate values (12, 13, 11, 2011) ;
insert into InfosCommDimDate values (13, 14, 11, 2011) ;
insert into InfosCommDimDate values (14, 15, 11, 2011) ;
insert into InfosCommDimDate values (15, 16, 11, 2011) ;
insert into InfosCommDimDate values (16, 17, 11, 2011) ;
insert into InfosCommDimDate values (17, 14, 12, 2011) ;
insert into InfosCommDimDate values (18, 15, 12, 2011) ;
insert into InfosCommDimDate values (19, 16, 12, 2011) ;
insert into InfosCommDimDate values (20, 17, 12, 2011) ;
insert into InfosCommDimDate values (21, 1, 8, 2012) ;
insert into InfosCommDimDate values (22, 2, 8, 2012) ;
insert into InfosCommDimDate values (23, 3, 8, 2012) ;
insert into InfosCommDimDate values (24, 13, 8, 2012) ;
insert into InfosCommDimDate values (25, 14, 8, 2012) ;
insert into InfosCommDimDate values (26, 15, 8, 2012) ;
insert into InfosCommDimDate values (27, 16, 8, 2012) ;
insert into InfosCommDimDate values (28, 17, 8, 2012) ;
insert into InfosCommDimDate values (29, 1, 9, 2012) ;
insert into InfosCommDimDate values (30, 2, 9, 2012) ;
insert into InfosCommDimDate values (31, 3, 9, 2012) ;
insert into InfosCommDimDate values (32, 13, 9, 2012) ;
insert into InfosCommDimDate values (33, 14, 9, 2012) ;
insert into InfosCommDimDate values (34, 15, 9, 2012) ;
insert into InfosCommDimDate values (35, 16, 9, 2012) ;
insert into InfosCommDimDate values (36, 17, 9, 2012) ;
insert into InfosCommDimDate values (37, 1, 10, 2012) ;
insert into InfosCommDimDate values (38, 2, 10, 2012) ;
insert into InfosCommDimDate values (39, 3, 10, 2012) ;
insert into InfosCommDimDate values (40, 13, 10, 2012) ;
insert into InfosCommDimDate values (41, 14, 10, 2012) ;
insert into InfosCommDimDate values (42, 15, 10, 2012) ;
insert into InfosCommDimDate values (43, 16, 10, 2012) ;
insert into InfosCommDimDate values (44, 17, 10, 2012) ;
insert into InfosCommDimDate values (45, 1, 11, 2012) ;
insert into InfosCommDimDate values (46, 2, 11, 2012) ;
insert into InfosCommDimDate values (47, 3, 11, 2012) ;
insert into InfosCommDimDate values (48, 13, 11, 2012) ;
insert into InfosCommDimDate values (49, 14, 11, 2012) ;
insert into InfosCommDimDate values (50, 15, 11, 2012) ;
insert into InfosCommDimDate values (51, 16, 11, 2012) ;
insert into InfosCommDimDate values (52, 17, 11, 2012) ;
insert into InfosCommDimDate values (53, 14, 12, 2012) ;
insert into InfosCommDimDate values (54, 15, 12, 2012) ;
insert into InfosCommDimDate values (55, 16, 12, 2012) ;
insert into InfosCommDimDate values (56, 17, 12, 2012) ;

insert into InfosCommFaitsPublications values (1, 2, 1);
insert into InfosCommFaitsPublications values (1, 2, 2);
insert into InfosCommFaitsPublications values (3, 2, 3);
insert into InfosCommFaitsPublications values (9, 7, 4);
insert into InfosCommFaitsPublications values (3, 8, 5);
insert into InfosCommFaitsPublications values (3, 12, 6);
insert into InfosCommFaitsPublications values (2, 13, 7);
insert into InfosCommFaitsPublications values (4, 42, 8);
insert into InfosCommFaitsPublications values (3, 55, 9);
insert into InfosCommFaitsPublications values (9, 56, 10);

insert into InfosCommFaitsLectures
	(num_seq_article,
	 num_seq_date_publi,
     num_seq_auteur,
     num_seq_date_invit,
     num_seq_date_lecture,
     num_seq_invitant,
     num_seq_lecteur)
values
	(4,	7, 9,	 7,		2,		9,		4),
	(4, 7, 9,	 7,		10,		9,		5),
	(4, 7, 9,	 8,	  null,		7,		null),
	(4, 7, 9,	 8,		9,		8,		7),
	(4, 7, 9,	 8,	   10,		8,		6),
	(4, 7, 9,	 8,		9,		8,		7),
	(4, 7, 9,	12,	   10,		8,		6),
	(4, 7, 9, null,	   11,	 null,		3;



