/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de creation :  14/12/2011 11:12:44                      */
/*==============================================================*/


drop table if exists InfosCommFaitsLectures;

drop table if exists InfosCommFaitsPublications;

drop table if exists InfosCommDimArticle;

drop table if exists InfosCommDimDate;

drop table if exists InfosCommDimMembre2;


/*==============================================================*/
/* Table : InfosCommDimArticle                                  */
/*==============================================================*/
create table InfosCommDimArticle
(
   num_seq_article      int auto_increment not null,
   numArticle           varchar(15) not null,
   rubrique             varchar(50) not null,
   primary key (num_seq_article)
);

/*==============================================================*/
/* Table : InfosCommDimDate                                     */
/*==============================================================*/
create table InfosCommDimDate
(
   num_seq_date         int auto_increment not null,
   jour                 int,
   mois                 int,
   annee                int,
   primary key (num_seq_date)
);

/*==============================================================*/
/* Table : InfosCommDimMembre2                                  */
/*==============================================================*/
create table InfosCommDimMembre2
(
   num_seq_membre       int auto_increment not null,
   pseudo               varchar(50) not null default 'defaut',
   ville                varchar(120),
   departement          char(2),
   anciennete           int,
   num_version			int not null default 1,
   debut_validite		date not null default '1900-01-01',
   fin_validite			date not null default '2199-01-01',
   primary key (num_seq_membre),
   unique(pseudo, num_version),
   unique(pseudo, debut_validite)
);

/*==============================================================*/
/* Table : InfosCommFaitsLectures                               */
/*==============================================================*/
create table InfosCommFaitsLectures
(
   num_seq_date_publi   int,
   num_seq_date_invit   int,
   num_seq_date_lecture int,
   num_seq_auteur       int,
   num_seq_invitant     int,
   num_seq_lecteur      int,
   num_seq_article      int,
);

/*==============================================================*/
/* Table : InfosCommFaitsPublications                           */
/*==============================================================*/
create table InfosCommFaitsPublications
(
   num_seq_membre       int,
   num_seq_date         int,
   num_seq_article      int
);

alter table InfosCommFaitsLectures add constraint FK_Reference_9910 foreign key (num_seq_article)
      references InfosCommDimArticle (num_seq_article) on delete restrict on update restrict;

alter table InfosCommFaitsLectures add constraint FK_Reference_994 foreign key (num_seq_date_publi)
      references InfosCommDimDate (num_seq_date) on delete restrict on update restrict;

alter table InfosCommFaitsLectures add constraint FK_Reference_995 foreign key (num_seq_date_invit)
      references InfosCommDimDate (num_seq_date) on delete restrict on update restrict;

alter table InfosCommFaitsLectures add constraint FK_Reference_996 foreign key (num_seq_date_lecture)
      references InfosCommDimDate (num_seq_date) on delete restrict on update restrict;

alter table InfosCommFaitsLectures add constraint FK_Reference_997 foreign key (num_seq_auteur)
      references InfosCommDimMembre2 (num_seq_membre) on delete restrict on update restrict;

alter table InfosCommFaitsLectures add constraint FK_Reference_998 foreign key (num_seq_invitant)
      references InfosCommDimMembre2 (num_seq_membre) on delete restrict on update restrict;

alter table InfosCommFaitsLectures add constraint FK_Reference_999 foreign key (num_seq_lecteur)
      references InfosCommDimMembre2 (num_seq_membre) on delete restrict on update restrict;

alter table InfosCommFaitsPublications add constraint FK_Reference_991 foreign key (num_seq_membre)
      references InfosCommDimMembre2 (num_seq_membre) on delete restrict on update restrict;

alter table InfosCommFaitsPublications add constraint FK_Reference_992 foreign key (num_seq_date)
      references InfosCommDimDate (num_seq_date) on delete restrict on update restrict;

alter table InfosCommFaitsPublications add constraint FK_Reference_993 foreign key (num_seq_article)
      references InfosCommDimArticle (num_seq_article) on delete restrict on update restrict;

