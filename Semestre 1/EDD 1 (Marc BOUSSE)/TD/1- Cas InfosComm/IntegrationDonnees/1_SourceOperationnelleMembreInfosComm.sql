-- Creation de la structure de InfosCommSourceMembre
drop table if exists InfosCommSourceMembre ;

create table InfosCommSourceMembre
(
  pseudo varchar(50),
  ville varchar(120),
  departement char(2),
  date_adhesion date,
  primary key (pseudo)
) ;

-- Remplissage de la table InfosCommSourceMembre
insert into InfosCommSourceMembre (pseudo, ville, departement, date_adhesion)
values
	('arrgh35',   'Rennes',       '35', '2014-10-21'),
	('bonjour53', 'Laval',        '53', '2014-10-21'),
	('georgette', 'Rennes',       '35', '2013-10-18'),
	('jean22',    'Caen',         '14', '2012-11-28'),
	('paul22',    'Saint Brieuc', '22', '2013-12-20'),
	('titi22',    'Lannion', 	  '22', '2012-12-06'),
	('toto35',    'Rennes',       '35', '2014-09-16'),
	('tutu53',    'Laval',        '53', '2014-05-20') ;

