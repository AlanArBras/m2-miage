-- Cas 1 : ajout d'une première version des nouveaux membres
insert into InfosCommDimMembre2 (pseudo, ville, departement, anciennete, num_version, debut_validite, fin_validite)
select pseudo, ville, departement, 0, 1, '1900-01-01', '2199-01-01'
from InfosCommSourceMembre
where pseudo not in
	(select pseudo
	 from InfosCommDimMembre2) ;
-- insère 2 rangées :
-- arrgh35 		Rennes 	35 	0 	1	1900-01-01	2199-01-01
-- bonjour53 	Laval 	53 	0 	1	1900-01-01	2199-01-01

-- Cas 2) : ajout de nouvelles versions pour les membres existants ayant changé
insert into InfosCommDimMembre2 (pseudo, ville, departement, anciennete, num_version, debut_validite, fin_validite)
select  source.pseudo, source.ville, source.departement,
		floor((datediff(now(), date_adhesion) / 365)),
		avant_derniere_version.num_version+1, now(), '2199-01-01'
from InfosCommSourceMembre as source
		inner join InfosCommDimMembre2 as avant_derniere_version
		on source.pseudo = avant_derniere_version.pseudo
where
    fin_validite = '2199-01-01'
	and
	(
		source.ville <> avant_derniere_version.ville
		or source.departement <> avant_derniere_version.departement
		or floor((datediff(now(), date_adhesion) / 365)) <> anciennete
	) ;
-- Insère 3 rangées :
-- georgette 	Rennes 	35 	1 	2 	(date du jour) 	2199-01-01
-- jean22 		Caen 	14 	1 	3 	(date du jour) 	2199-01-01
-- tutu53 		Laval 	53 	0 	2 	(date du jour) 	2199-01-01

-- 2bis : ll faut ensuite modifier la date de fin de validité des anciennes dernières versions
-- (celles qui ont reçu un successeur suite au Insert précédent)
update InfosCommDimMembre2 as avant_derniere_version
set fin_validite = now()
where
	date(debut_validite) <> date(now()) 
	and fin_validite = '2199-01-01'
	and pseudo in
		(select pseudo from InfosCommSourceMembre as source
		 where
			source.ville <> avant_derniere_version.ville
			or source.departement <> avant_derniere_version.departement
			or floor((datediff(now(), date_adhesion) / 365)) <> anciennete
		) ;
-- 3 rangées sont modifiées comme suit :
-- georgette 	Rennes			35 	0 	1 	2013-10-18 	(date du jour)
-- jean22 		Saint Brieuc 	22 	1 	2 	2013-11-28 	(date du jour)
-- tutu53 		Rennes 			35 	0 	1 	2014-05-20 	(date du jour)

